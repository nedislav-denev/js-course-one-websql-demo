/**
 * Master application
 * Includes Classes Env, App
 */
/*
* TO DO
* cache login input fields - DONE
* create validation and check if username/pass match - DONE
* change to document view after login - DONE
* Document ALL THE THINGS! - Done
* Add a logout button - DONE
* refactor landing viewDocuments and cache the elements used there - DONE
* LOAD DOCUMENTS FOR CURRENT USER
* ADD A NEW DOCUMENT
* DELETE A DOCUMENT
* EDIT A DOCUMENT
*/

window.App = window.App || {};

// Where do we use this?
// window.Env = window.Env || {};

(function($, App, API){

    // Define private Objects for the App
    var User, userStatus, UI = {};

    // This fires up all the inner App functionality
    App.init = function initializeApplication () {
        User.init();
        UI.init();
        App.changeStates();
    };

    // Set default App.sate
    App.state = null;

    // Define the App.states
    App.states = {
        'default': function notLoggedInUser () {
            /*
            * REFACTOR THIS TO BE DONE BY UI
            */
            // Show initial state DOM wrappers
            UI.elem.logRegEl.style.display = 'block';
            UI.elem.docWrapper.style.display = 'none';
        },

        /***
         * Fire up Document view
         * returns void
         */
        'viewDocuments': function viewUserDocuments () {
            alert('Welcome, ' + localStorage.username );
             /*
            * REFACTOR THIS TO BE DONE BY UI
            */
            // Show initial state DOM wrappers
            UI.elem.docWrapper.style.display = 'block';
            UI.elem.logRegEl.style.display = 'none';
        }
    };

    /***
     * Determines the user loggedIn state
     * returns {String} 'viewDocuments'/'default'
     */
    App.getCurrentState = function getAppCurrentState () {
        return User.loggedIn ? 'viewDocuments' : 'default';
    };

    /***
     * Changes up between the app.state [default/viewDocuments]
     * returns void
     */
    App.changeStates = function changeAppState () {
        // Cache the current state
        var state = this.getCurrentState();

        if ( this.state !== state ) {
            this.state = state;
            if ( this.state ) {
                // Unbind the events for the previous state
                UI.events.unbind[this.state]();
            }
            // Bind the events for the current state
            UI.events.bind[state]();
        }
        // Fire up initial methods for the current state
        App.states[state]();
    };


    App.callbacks = {
        /***
         * Logs in the user if the input fields are filled in and the user exists in the WebSQL DB.
         * @param {Object} result
         * returns void
         */
        'login': function loginCallback (result) {
            if ( result.length ) {
                // Save login data to localStorage
                userStatus.set(result[0].username, result[0].id);
                // Change user state
                User.loggedIn = true;
                // fire changeStates
                App.changeStates();
            } else {
                alert('Wrong username or password! \nPlease try again!');
                // Clear login inputs' values
                UI.elem.loginUsername.value = null;
                UI.elem.loginPass.value = null;
            }
        },

        /***
         * Registers the user if it isn't present in the WebSQL DB, otherwise redirects to LOGIN State
         * @param {Object} result
         * returns void
         */
        'register': function registerCallback (result) {
            //user is present or good to go - register
            if( !( result.length ) ) {
                // Add user to WebSQL
                API.user.add(UI.elem.registerUsername.value, UI.elem.registerPass.value);
                alert('You\'ve registered successfully!');
                // Change form to login
                UI.currentForm = 'login';
                UI.toggleForm(UI.elem.loginNav, UI.elem.loginTab, UI.elem.registerNav, UI.elem.registerTab);
            } else {
                alert('This user already exists, please login.');
                // Change form to login
                UI.currentForm = 'login';
                UI.toggleForm(UI.elem.loginNav, UI.elem.loginTab, UI.elem.registerNav, UI.elem.registerTab);
            }
        }
    };

    /***
     * Attempt to login user (This function is fired only onClick)
     * returns callback whether the username and password are present in the WebSQL DB or error if the data in the input fields in not set properly
     * returns void
     */
    App.loginUser = function() {
        // Cache values from the input fields
        var username = UI.elem.loginUsername.value,
            password = UI.elem.loginPass.value;

        if ( username && password ) {
            // Check WebSQL DB for the user
            App.checkUser( username, password );
            console.log('Attempting to log in...');
        } else {
            alert('Enter username and/or password!');
        }
    };

    /***
     * Attempt to register user (This function is fired only onClick)
     * returns callback whether the username and password are present in the WebSQL DB or error if the data in the input fields in not set properly
     * returns void
     */
    App.registerUser = function () {
        // Cache values from the input fields
        var username = UI.elem.registerUsername.value,
            password = UI.elem.registerPass.value,
            repeatPassword = UI.elem.registerRptPass.value;

        // Check if all inputs are filled in and passwords match
        if ( username && password && repeatPassword && ( password === repeatPassword ) && ( password.length >= 5 ) ) {
            // Check WebSQL DB for the user
            App.checkUser( username, password );
        } else {
            alert('ENTER YOUR DATA CORRECTLY\nAND\nUSE AN ATLEAST 5 CHARACTER LONG PASSWORD!');
        }

    };

     /***
     * Logs out the user! (This function is fired only onClick)
     * returns void
     */
    App.logoutUser = function() {
        alert('See you soon, ' + localStorage.username + ' :)');

        // Clear out the localStorage
        userStatus.set( '' , '' );

        // Set user to logged out statea
        User.loggedIn = false;

        // Set default form to login
        UI.currentForm = 'login';

        // fire changeStates
        App.changeStates();
    };

    /***
     * Check if user exists in the WebSQL DB
     * @param {String} username
     * @param {String} password
     * returns callback whether the username and password are present in the WebSQL DB
     */
    App.checkUser = function ( username, password ) {
        API.user.check(username, password, App.callbacks[UI.currentForm]);
    };

    UI.events = {
        bind: {
            /***
             * Binds all the 'default' event handlers
             * returns void
             */
            'default': function bindDefaultUIEvents () {
                // body...
                UI.elem.loginNav.addEventListener('click', UI.changeForms);
                UI.elem.registerNav.addEventListener('click', UI.changeForms);
                UI.elem.registerButton.addEventListener('click', App.registerUser);
                UI.elem.loginButton.addEventListener('click', App.loginUser);

            },
            /***
             * Binds all the 'viewDocuments' event handlers
             * returns void
             */
            'viewDocuments': function bindViewDocumentsEvents () {
                // body...
                UI.elem.userLogout.addEventListener('click', App.logoutUser);
            }
        },
        unbind: {

            /***
             * Unbinds all the 'default' event handlers
             * returns void
             */
            'default': function unbindDefaultUIEvents () {
                // body...
                UI.elem.loginNav.removeEventListener('click', UI.changeForms);
                UI.elem.registerNav.removeEventListener('click', UI.changeForms);
                UI.elem.registerButton.removeEventListener('click', App.registerUser);
                UI.elem.loginButton.removeEventListener('click', App.loginUser);
            },
            /***
             * Unbinds all the 'viewDocuments' event handlers
             * returns void
             */
            'viewDocuments': function unbindViewDocumentsEvents () {
                // body...
                UI.elem.userLogout.removeEventListener('click', App.logoutUser);
            }
        }
    };

    // Store all the DOM elements here
    UI.elem = {};

    /***
     * Caches all the DOM elements
     * returns void
     */
    UI.init = function initializeUI () {
        // Cache UI elements

        /* General DOM Elements*/
        this.elem.docWrapper = document.getElementById('document-wrapper');
        this.elem.logRegEl = document.getElementById('login-register-forms');

        /* Login/Register related DOM Elements*/
        this.elem.registerNav = document.getElementById('js-register-nav');
        this.elem.registerNavHash = document.getElementById('js-register-nav').getElementsByTagName('a')[0].hash;
        this.elem.registerTab = document.getElementById('register-tab');
        this.elem.loginNav = document.getElementById('js-login-nav');
        this.elem.loginNavHash = document.getElementById('js-login-nav').getElementsByTagName('a')[0].hash;
        this.elem.loginTab = document.getElementById('login-tab');
        this.elem.registerButton = document.getElementById('register-button');
        this.elem.registerUsername = document.getElementById('register-username');
        this.elem.registerPass = document.getElementById('register-password');
        this.elem.registerRptPass = document.getElementById('register-rpt-password');
        this.elem.loginButton = document.getElementById('login-button');
        this.elem.loginUsername = document.getElementById('login-username');
        this.elem.loginPass = document.getElementById('login-password');

        /* documentView related DOM Elements*/
        this.elem.userLogout = document.getElementById('js-logout-nav');
    };

    // Set default form to register
    UI.currentForm = 'register';

    /***
     * Switch between forms on click (This function is fired only onClick)
     * @param {Click Event} evt
     * returns void
     */
    UI.changeForms = function loginOrRegister ( evnt ) {

        if ( evnt.target.hash == UI.elem.registerNavHash ) {
            UI.currentForm = 'register';
            UI.toggleForm(UI.elem.registerNav, UI.elem.registerTab, UI.elem.loginNav, UI.elem.loginTab);
        } else if ( evnt.target.hash == UI.elem.loginNavHash ) {
            UI.currentForm = 'login';
            UI.toggleForm(UI.elem.loginNav, UI.elem.loginTab, UI.elem.registerNav, UI.elem.registerTab);
        }
    };

    /***
     * Toggle between login and register form
     * @param {DOM Element} elNavActivate
     * @param {DOM Element} elTabActivate
     * @param {DOM Element} elNavDeactivate
     * @param {DOM Element} elTabDeactivate
     * returns void
     */
    UI.toggleForm = function formToggler ( elNavActivate, elTabActivate, elNavDeactivate, elTabDeactivate ) {
        elNavActivate.classList.add('active');
        elTabActivate.classList.add('active');
        elNavDeactivate.classList.remove('active');
        elTabDeactivate.classList.remove('active');
    };

    /*
     * User Object
    */
    User = {
        loggedIn: false,
        settings: {
            username: null,
            password: null
        },

        /***
         * Checks user status and sets the loggedIn state
         * returns void
         */
        init: function initializeUser () {
            this.settings = userStatus.get();
            this.loggedIn = this.settings.username && this.settings.userid ? true : false;
        }

    };

    userStatus = {

        /***
         * Set the username and user ID
         * @param {string} username
         * @param {string} userid
         * returns void
         */
        set: function( username, userid ) {
            // localStorage check if there is an existing username and userid
            localStorage.username = username;
            localStorage.userid = userid;
        },
        /***
         * @param {string} username
         * @param {string} userid
         * returns object with username and userid
         */
        get: function () {
            return {
                username: localStorage.username,
                userid: localStorage.userid
            };
        }
    };

    /**
     * Document ready
     * FIRE THIS THING UP!
     */
    $(function() {
        App.init();
    });

})(jQuery, App, API);